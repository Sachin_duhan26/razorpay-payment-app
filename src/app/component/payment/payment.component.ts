import { Component, OnInit } from '@angular/core';
import { WindowRefService } from "../../service/window-ref.service";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
  providers: [WindowRefService]
})
export class PaymentComponent implements OnInit {

  constructor(private winRef: WindowRefService) { }
  ngOnInit() { }
  
  options: any = {
    "key": "rzp_test_AMqRN7Mx6qvI5J",
    "amount": 100,
    "name": "Sahi price online",
    "description": "payment for Honda amaze petrol",
    "modal": {
      "escape": false
    },
    "prefill": {
      "name": "Sachin duhan",
      "contact": 8586821051,
      "email": "duhan.sachin@gmail.com",
    },
    "handler": function (response) {
      console.log(response);
      alert(response.razorpay_payment_id);
    },
    "notes": {
      "address": "DTU FLAT-31 type-3 delhi-42"
    },
    "theme": {
      "color": "#6fbc29"
    }
  };

  rzp2: any;

  public initPay(): void {
    this.rzp2 = new this.winRef.nativeWindow.Razorpay(this.options);
    this.rzp2.open();
  }
}
